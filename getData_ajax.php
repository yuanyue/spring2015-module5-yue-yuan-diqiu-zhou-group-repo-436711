<?php
require 'database.php';
header("Content-Type: application/json");
$token=htmlentities($_POST['token']);
ini_set("session.cookie_httponly", 1);
session_start();
if($_SESSION['token']==$token){
$_SESSION['username']="yy";
	$year = (int)htmlentities($_POST['year']);
	$month = (int)htmlentities($_POST['month']);
	$date = (int)htmlentities($_POST['date']);
	$stmt=$mysqli->prepare("select text, title from event where year=? and month=? and date=? and (author=? or viewer=?)");
	//query needs to be modified;
	if(!$stmt){
		echo json_encode(array(
			"success" => false,
			"exist"=>false,
			"message" => "No event"
		));
		exit;
	}
	$stmt->bind_param('iiiss', $year,$month,$date,$_SESSION['username'],$_SESSION['username']);
	$stmt->execute();
	$result=$stmt->get_result();
	$array = array();
	while($row = $result->fetch_assoc()){
		$event = array(
			"title" => htmlentities($row["title"]),
			"text" => htmlentities($row["text"]),
		);
		array_push($array, $event);
		
	}	
	echo json_encode(array("eventarr"=>$array,
			"success" => true));
	$stmt->close();
	exit;
}else{
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect user or CSRF token"
	));
	exit;
}
?>