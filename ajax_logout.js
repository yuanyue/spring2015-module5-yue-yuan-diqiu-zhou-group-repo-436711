function logoutAjax(event){
	var xmlHttp = new XMLHttpRequest();
	var username=document.getElementById("username");
	var password=document.getElementById("password");
	xmlHttp.open("POST", "logOut_ajax.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load",  function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			alert("You've been logged out!");

		}else{
			alert("Log out process failed"+jsonData.message);
		}
	},false);
	xmlHttp.send(null);
}
document.getElementById("logout_btn").addEventListener("click", logoutAjax, false);