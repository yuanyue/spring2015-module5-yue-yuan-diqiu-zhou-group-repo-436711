function addViewerAjax(event){
	var viewername = document.getElementById("viewername").value;
	var username = document.getElementById("username").value;
	var token = document.getElementById("token").value;
	var date = document.getElementById("date").value;
	var month = document.getElementById("month").value;
	var year = document.getElementById("year").value;
	var dataString = "username=" + encodeURIComponent(username) +"&viewername="+encodeURIComponent(viewername)+ "&token="+ encodeURIComponent(token) + "&date=" + encodeURIComponent(date) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "addViewer_ajax.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load",  function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			alert("You've sucessfully added the viewer!");
		}else{
			alert("Adding viewer failed"+jsonData.message);
		}
	},false);
	xmlHttp.send(dataString);
}
document.getElementById("add_btn").addEventListener("click", addViewerAjax, false);