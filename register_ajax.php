<?php
require 'database.php';
header("Content-Type: application/json");
$username=$_POST['username'];
$password=$_POST['password'];
$crypted_password=crypt($password);
$stmt = $mysqli->prepare("insert into loginfo (username, password, crypted_password) values (?,?,?)");
if(!$stmt){
    echo json_encode(array(
    	"success" => false,
    	"message" => $mysqli->error
    ));
}
            // Bind the parameter
$stmt->bind_param('sss', $username, $password, $crypted_password);
$stmt->execute();
$stmt->close();
           
ini_set("session.cookie_httponly", 1);
session_start();
$_SESSION['username'] = $username;
$_SESSION['token'] = substr(md5(rand()),0,10);
//maybe need a header location

echo json_encode(array(
	"success" => true,
	"token" => $_SESSION['token']
));
exit;

?>