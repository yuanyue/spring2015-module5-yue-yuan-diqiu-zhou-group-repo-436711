function loginAjax(event){
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	var token   = document.getElementById("token");
	var dataString = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "login_ajax.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load",  function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			token.setAttribute("value",jsonData.token);
			alert("You've been Logged in!");
		}else{
			alert("Login process failed"+jsonData.message);
		}
	},false);
	xmlHttp.send(dataString);
}
document.getElementById("login_btn").addEventListener("click", loginAjax, false);